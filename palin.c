#include <stdio.h>

int main(){
    
/*
Author: Allan Hawkins 
Clase: CSC3320
Prof.: Xi He

This program checks if a phrase is palindrome using integer index
*/

	char str[100];
	int i=0;
	int j;
	int k;
	int result=0;
    	char ch;

    	printf("Enter posible palindrome: ");
    
    	while((ch = getchar()) != '\n'){
		if(ch < 65 || (ch > 90 && ch < 97) || ch > 122)
			ch = getchar();
		if(ch == '\n')
		break;
		str[i] = ch;
		i++;
	
	}       	

    	for(k=0,j=(i-1);j>=0;k++,j--){
        	if(str[k]!=str[j]){
            		result=1;
       		 }		
   	}	
        
    	if(result == 0)
        	printf("Palindrome\n");
   	else
        	printf("Not a palindrome\n");

}
