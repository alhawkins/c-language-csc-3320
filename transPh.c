#include <stdio.h>
#include <ctype.h>

/*
This program converts alphabetic number into numeric.
Author: Allan Hawkins 
Class: CSC 3320
Prof: Xi He
*/

	int main(){
	
	char ch;

	printf("Enter your alphabetic phone number: ");
	ch = getchar();
	
	while (ch != '\n'){
	
	if (!((ch>96 && ch<123)||(ch>64&&ch<91)))
	printf("%c",ch);
	
	switch(toupper(ch)){
	case 65: printf("2");break;
	case 66: printf("2");break;
	case 67: printf("2");break;
	case 68: printf("3");break;
	case 69: printf("3");break;
	case 70: printf("3");break;
	case 71: printf("4");break;
	case 72: printf("4");break;
	case 73: printf("4");break;
	case 74: printf("5");break;
	case 75: printf("5");break;
	case 76: printf("5");break;
	case 77: printf("6");break;
	case 78: printf("6");break;
	case 79: printf("6");break;
	case 80: printf("7");break;
	case 82: printf("7");break;
	case 83: printf("7");break;
	case 84: printf("8");break;
	case 85: printf("8");break;
	case 86: printf("8");break;
	case 87: printf("9");break;
	case 88: printf("9");break;
	case 89: printf("9");break;
	}
	ch = getchar();
	}
	printf("\n");
	return 0;

	}
