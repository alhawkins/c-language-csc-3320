#include <stdio.h>


/*
Author: Allan Hawkins 
Clase: CSC3320
Prof.: Xi He

This program checks if a phrase is palindrome using pointers
*/
int main(){    

	char str[100];
	char str2[100];
	char *s;
	char *s2;
	s = str;
	s2 = str2;
	int i = 0;
	int result = 0;
    	char ch;

    	printf("Enter posible palindrome: ");
    
    	while((ch = getchar()) != '\n'){
		if(ch < 65 || (ch > 90 && ch < 97) || ch > 122)
			ch = getchar();
		if(ch == '\n')
		break;
		*s = ch;
		s++;
		s2++;
		i++;
	}       	
	*s = '\0';
	
	for(s=str,s2--;*s != '\0';s++,s2--){
		*s2 = *s;
	}
	s2=str2+i;
	*s2 = '\0';


    	for(s=str,s2=str2;(*s != '\0') && (*s2 != '\0');s++,s2++){
        	if(*s != *s2){
            		result=1;
       		 }		
   	}	
        
    	if(result == 0)
        	printf("Palindrome\n");
   	else
        	printf("Not a palindrome\n");

}
