void printMenu();
void insStudent(Snodeptr students);
void delStudent(Snodeptr students, Cnodeptr courses);
void insCourse(Cnodeptr courses);
void delCourse(Snodeptr students, Cnodeptr courses);
void insEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses);
void delEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses);
void prnClassList(Cnodeptr courses);
void prnTranscript(Enodeptr enroll, Snodeptr students);
void readFile(Snodeptr students, Cnodeptr courses);
/* prompts user for name of file and reads entire database from the
   file into the data structure */

void writeFile(Snodeptr students, Cnodeptr courses);
/* prompts user for name of file and writes entire datastructure
   to the file */

int getline(char line[], int maxline);
