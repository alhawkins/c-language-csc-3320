Enodeptr eAlloc(void);
/* Allocates storage for enode structure and returns pointer to structure */

int memberEnrolls(struct studentType x,
                  struct courseType y,
                  Snodeptr students,
                  Cnodeptr courses,
                  Enodeptr *es,
                  Enodeptr *ec);
/* returns 0 if the student x is enrolled in course y; 1 otherwise;
   it also returns a pointer (in variable es) to previous enode in the
   student's list where it found the enrolls structure and a pointer
   (in variable ec) to previous enode in the course's list where it
   found the enrolls structure; if it did not find the enrolls 
   structure, no value is returned in es and ec. */
   
int insertEnrolls(struct studentType x,
                  struct courseType y,
                  char grade,
                  Enodeptr enroll,
		  Snodeptr students,
                  Cnodeptr courses);
/* inserts the fact that student x has obtained grade in course y;
   returns 0 if successful and 1 if failed */

int deleteEnrolls(struct studentType x,
                  struct courseType y,
                  Enodeptr enroll,
		  Snodeptr students,
                  Cnodeptr courses);
/* deletes the fact that student x obtained grade in course y;
   returns 0 if successful and 1 if failed */
   
int printClassList(struct courseType x,
                   Cnodeptr courses);
/* Given course x, it prints the sid and names of students enrolled
   in the course along with their grades; pretty print with -more- like
   option. returns 0 if successful and 1 if failed */

int printTranscript(struct studentType x,
                   Enodeptr enroll, 
		   Snodeptr students);
/* Given student x, it prints the cno, title and hours of courses taken by
   the student; pretty print with -more- like
   option. returns 0 if successful and 1 if failed */

void printEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses);
/* prints list of all enrollments; pretty print with -more- like options */

