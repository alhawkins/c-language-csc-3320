#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"
#include "courses.h"
#include "enrolls.h"

Cnodeptr cAlloc(void) {
  return (Cnodeptr) malloc(sizeof(struct cnode));
}

void makenullCourses(Cnodeptr *courses) {
  (*courses)=cAlloc();	
}

int memberCourses(struct courseType x, Cnodeptr courses, Cnodeptr *p) {
  Cnodeptr cur, prev;

  for(cur=courses->next, prev=courses;cur!=NULL && strcmp(cur->course.cno,x.cno); prev=cur, cur=cur->next);
  if(cur==NULL){
    (*p)=prev;
    return 1;	
  }  		
  (*p)=cur;
  return 0;
}

int insertCourses(struct courseType x, Cnodeptr courses) {
  Cnodeptr new_node;
  new_node = malloc(sizeof(struct cnode));
  if(new_node==NULL){
    return 1;
  }
  new_node->course=x;
  new_node->next=NULL;
  
  if(courses->next==NULL){
    courses->next=new_node;
  }
  else{
    while(courses->next!=NULL)
	courses=courses->next;

	courses->next=new_node;
  }
  return 0;
}

int deleteCourses(struct courseType x, Snodeptr students, Cnodeptr courses) {
	Cnodeptr cur, prev;
  
 	for(cur=courses->next, prev=courses;cur!=NULL && strcmp(cur->course.cno,x.cno); prev=cur, cur=cur->next);
	
	if(prev->next==NULL){
	  return 1;
	}
	prev->next=cur->next;
	free(cur);
  return 0;
}

void printCourses(Cnodeptr courses) {
	int count=0;
	Cnodeptr p=courses;
	char ch;
	while((p->next)!=NULL){
		printf("cno=%s,title=%s\n",p->next->course.cno,p->next->course.title);
		p=p->next;
		}

}
