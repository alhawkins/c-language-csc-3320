#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"
#include "students.h"
#include "courses.h"
#include "enrolls.h"

Enodeptr eAlloc(void) {
  return (Enodeptr) malloc(sizeof(struct enode));
}

void makenullEnroll(Enodeptr *enroll) {
  (*enroll)=eAlloc();	
}


int memberEnrolls(struct studentType x,
                  struct courseType y,
                  Snodeptr students,
                  Cnodeptr courses,
                  Enodeptr *es,
                  Enodeptr *ec) {
	
 	
  	
  return 0;
}


int insertEnrolls(struct studentType x,
                  struct courseType y,
                  char grade,
		  Enodeptr enroll,
		  Snodeptr students, 
                  Cnodeptr courses) {
  
		Enodeptr new_node;
		Snodeptr s;
		Cnodeptr c;

		new_node = malloc(sizeof(struct enode));
		if(new_node==NULL){
		return 1;
		}

		new_node->sNext=NULL;
		new_node->cNext=NULL;
		
		memberStudents(x, students, &s);
		memberCourses(y, courses, &c);
 		
		new_node->sOwner=s;
		new_node->cOwner=c;		

  		if(enroll->sNext==NULL){
    		enroll->sNext=new_node;
  		}
  		else{
    		while(enroll->sNext!=NULL)
		enroll=enroll->sNext;

		enroll->sNext=new_node;
 	 	}		
	

  return 0;
}


int deleteEnrolls(struct studentType x,
                  struct courseType y,
                  Enodeptr enroll,
		  Snodeptr students,
                  Cnodeptr courses) {
	
	Enodeptr cur, prev;
	
	for(cur=enroll->sNext, prev=enroll;prev->sNext!=NULL && strcmp(cur->sOwner->student.sid,x.sid); prev=cur, cur=cur->sNext);
	if(prev->sNext==NULL){
	  return 1;
	}
	prev->sNext=cur->sNext;
	free(cur);
  return 0;
}


int printClassList(struct courseType x, Cnodeptr courses) {
	int count=0;
	Cnodeptr p=courses;
	char ch;
	while((p->next)!=NULL){
		printf("title=%s\n",p->next->course.title);
		p=p->next;
		}
  return 0;
}

int printTranscript(struct studentType x, Enodeptr enroll, Snodeptr students) {
	int count=0;
	Snodeptr p=students;
	Enodeptr e=enroll;
	char ch;
	while((p->next)!=NULL){
		printf("name=%s\ngrade=%d\n ",p->next->student.sname,e->sNext->grade);
		p=p->next;
		e=e->sNext;
		}
  return 0;
}

void printEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses) {
	int count=0;
	Enodeptr p=enroll;
	char ch;
	while((p->sNext)!=NULL){
		printf("sid=%s,name=%s\n",p->sNext->sOwner->student.sid,p->sNext->sOwner->student.sname);
		p=p->sNext;
		}
}

