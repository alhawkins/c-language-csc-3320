#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "types.h"
#include "students.h"
#include "courses.h"
#include "enrolls.h"
#include "db.h"

main() {
  Snodeptr students;
  Cnodeptr courses;
  Enodeptr enroll;
  char line[80];
  char ch;
  int done;

  makenullStudents(&students);
  makenullCourses(&courses);
  makenullEnroll(&enroll);

  do {
    printMenu();
    printf("Enter your option: ");
    getline(line,sizeof(line));
    sscanf(line,"%c",&ch);
    switch (ch) {
      case '0': insStudent(students);
                printf("\n");
                break;
      case '1': delStudent(students,courses);
                printf("\n");
                break;
      case '2': printStudents(students);
                printf("Press RETURN to continue"); getchar();
                printf("\n");
                break;
      case '3': insCourse(courses);
                printf("\n");
                break;
      case '4': delCourse(students,courses);
                printf("\n");
                break;
      case '5': printCourses(courses);
                printf("Press RETURN to continue"); getchar();
                printf("\n");
                break;
      case '6': insEnrolls(enroll,students,courses);
                printf("\n");
                break;
      case '7': delEnrolls(enroll,students,courses);
                printf("\n");
                break;
      case '8': prnTranscript(enroll,students);
                printf("Press RETURN to continue"); getchar();
                printf("\n");
                break;
      case '9': prnClassList(courses);
                printf("Press RETURN to continue"); getchar();
                printf("\n");
                break;
      case 'a': case 'A':
                printEnrolls(enroll,students,courses);
                printf("Press RETURN to continue"); getchar();
                printf("\n");
                break;
      case 'b': readFile(students,courses);
                break;
      case 'c': writeFile(students,courses);
                break;
      case 'q': case 'Q': 
                done = 1;
                break;
      default:  printf("Enter option again\n");
                break;
    }
  } while (!done);
}

void printMenu() {
  printf("\n\n");
  printf("0. INSERT STUDENT      3. INSERT COURSE \n");
  printf("1. DELETE STUDENT      4. DELETE COURSE \n");
  printf("2. PRINT STUDENTS      5. PRINT COURSES \n\n");
  printf("6. INSERT ENROLLS      b. READ FROM FILE \n");
  printf("7. DELETE ENROLLS      c. WRITE TO FILE \n");
  printf("8. PRINT TRANSCRIPT \n");
  printf("9. PRINT CLASS LIST \n");
  printf("a. PRINT ENROLLS \n\n");
  printf("q. QUIT\n");
  printf("\n\n");
}

void insStudent(Snodeptr students) {
  struct studentType x;
  char line[80];

  printf("Enter Student Id: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sid);
  printf("Type in Student Name: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sname);
  if (insertStudents(x,students) == 0)
    printf("Student %10s has been inserted", x.sid);
  else
    printf("Student %10s already exists", x.sid);
}

void delStudent(Snodeptr students, Cnodeptr courses) {
  struct studentType x;
  char line[80];

  printf("Enter sid to be deleted: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sid);
  if (deleteStudents(x,students,courses) == 0)
    printf("Student %10s has been deleted", x.sid);
  else
    printf("Student %10s does not exist", x.sid);
}

void insCourse(Cnodeptr courses) {
  struct courseType x;
  char line[80], hrs[10];

  printf("Enter Course Number: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.cno);
  printf("Enter Course Title: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.title);
  printf("Enter Hours: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",hrs);
  x.hours = atoi(hrs);
  if (insertCourses(x,courses) == 0)
    printf("Course (%10s,%10s,%d) has been inserted", x.cno, x.title, x.hours);
  else
    printf("Course %10s already exists", x.cno);
}

void delCourse(Snodeptr students, Cnodeptr courses) {
  struct courseType x;
  char line[80];

  printf("Enter cno to be deleted: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.cno);
  if (deleteCourses(x,students,courses) == 0)
    printf("Course %10s has been deleted", x.cno);
  else
    printf("Course %10s does not exist", x.cno);
}

void insEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses) {
  struct studentType x;
  struct courseType y;
  char line[80], grade;

  printf("Enter sid: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sid);
  printf("Enter cno: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",y.cno);
  printf("Enter grade: ");
  getline(line,sizeof(line));
  grade = line[0];
  if (insertEnrolls(x,y,grade,enroll,students,courses) == 0)
     printf("Student %10s has been enrolled in %10s with grade %c\n", 
            x.sid, y.cno, grade);
  else
     printf("Student %10s could not be enrolled in %10s with grade %c\n", 
            x.sid, y.cno, grade);
}

void delEnrolls(Enodeptr enroll, Snodeptr students, Cnodeptr courses) {
  struct studentType x;
  struct courseType y;
  char line[80];

  printf("Enter sid: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sid);
  printf("Enter cno: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",y.cno);
  if (deleteEnrolls(x,y,enroll,students,courses) == 0)
     printf("Student %10s has been dropped from %10s\n", x.sid, y.cno);
  else
     printf("Student %10s could not be dropped from %10s\n", x.sid, y.cno);
}

void prnClassList(Cnodeptr courses) {
  struct courseType x;
  char line[80];  

  printf("Enter cno: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.cno);
  if (printClassList(x,courses) == 1)
    printf("Course %10s does not exist\n",x.cno);
}

void prnTranscript(Enodeptr enroll, Snodeptr students) {
  struct studentType x;
  char line[80];

  printf("Enter sid: ");
  getline(line,sizeof(line));
  sscanf(line,"%s",x.sid);
  if (printTranscript(x,enroll,students) == 1)
    printf("Student %10s does not exist\n",x.sid);
}

void readFile(Snodeptr students, Cnodeptr courses) {
/* 
  FILE *fp;
  char sid[50];
  char sname[50];
  char cno[50];
  char title[50];
  int nStudents;
  int nCourses;
  int nEnroll;
  int i;

  fp = fopen("data.dat", "r");

  fscanf(fp,"%d",&nStudents);
  Snodeptr *students;
  makenullStudents(&students);
  
  for(i=0;i<nStudents;i++){
  fscanf(fp,"%s",sid);
  fscanf(fp,"%s",sname);

  Snodeptr new_node;
  new_node = malloc(sizeof(struct snode));
  new_node->student.sid=sid;
  new_node->student.sname=sname;
  new_node->next=NULL;
  
  if(students->next==NULL){
    students->next=new_node;
  }
  else{
    while(students->next!=NULL)
	students=students->next;

	students->next=new_node;
  }

  }

  fscanf(fp,"%d",&nCourses); 
  Cnodeptr *courses;
  makenullStudents(&courses);
  
  for(i=0;i<nCourses,i++){
  fscanf(fp,"%s",cno);
  fscanf(fp,"%s",title);

  Cnodeptr new_node;
  new_node = malloc(sizeof(struct cnode));

  new_node->course.con=cno;
  new_node->course.title=title;
  new_node->next=NULL;
  
  if(courses->next==NULL){
    courses->next=new_node;
  }
  else{
    while(courses->next!=NULL)
	courses=courses->next;

	courses->next=new_node;
  }
  }

  fscanf(fp,"%d",&nEnroll);
  Enodeptr *enroll;
  makenullStudents(&enroll);
  
  for(i=0;i<nEnroll,i++){
		Enodeptr new_node;
		Snodeptr s;
		Cnodeptr c;

		new_node = malloc(sizeof(struct enode));
		if(new_node==NULL){
		return 1;
		}

		new_node->sNext=NULL;
		new_node->cNext=NULL;
		
		memberStudents(x, students, &s);
		memberCourses(y, courses, &c);
 		
		new_node->sOwner=s;
		new_node->cOwner=c;		

  		if(enroll->sNext==NULL){
    		enroll->sNext=new_node;
  		}
  		else{
    		while(enroll->sNext!=NULL)
		enroll=enroll->sNext;

		enroll->sNext=new_node;
 	 	}		
  }
*/
}

void writeFile(Snodeptr students, Cnodeptr courses) {

}

int getline(char s[], int lim) {
  int c, i;

  for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; ++i)
    s[i] = c;
  if (c == '\n') {
    s[i] = c;
    ++i;
  }
  s[i] = '\0';
  return i;
}
