#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "types.h"
#include "students.h"
#include "enrolls.h"

Snodeptr sAlloc(void) {
  return (Snodeptr) malloc(sizeof(struct snode));
}

void makenullStudents(Snodeptr *students) {
  (*students)=sAlloc();	
}

int memberStudents(struct studentType x, Snodeptr students, Snodeptr *s) {
  Snodeptr cur, prev;

  for(cur=students->next, prev=students;prev->next!=NULL && strcmp(cur->student.sid,x.sid); prev=cur, cur=cur->next);
  if(prev->next==NULL){
    (*s)=prev;
    return 1;	  		
  }
  (*s)=cur;
	
  return 0;
}

int insertStudents(struct studentType x, Snodeptr students) {
  Snodeptr new_node;
  new_node = malloc(sizeof(struct snode));
  if(new_node==NULL){
    return 1;
  }
  new_node->student=x;
  new_node->next=NULL;
  
  if(students->next==NULL){
    students->next=new_node;
  }
  else{
    while(students->next!=NULL)
	students=students->next;

	students->next=new_node;
  }

  return 0;
}

int deleteStudents(struct studentType x, Snodeptr students, Cnodeptr courses) {
	Snodeptr cur, prev;
	
	for(cur=students->next, prev=students;prev->next!=NULL && strcmp(cur->student.sid,x.sid); prev=cur, cur=cur->next);
	if(prev->next==NULL){
	  return 1;
	}
	prev->next=cur->next;
	free(cur);
  return 0;
}

void printStudents(Snodeptr students) {

	int count=0;
	Snodeptr p=students;
	char ch;
	while((p->next)!=NULL){
		printf("sid=%s,name=%s\n",p->next->student.sid,p->next->student.sname);
		p=p->next;
		}
}
