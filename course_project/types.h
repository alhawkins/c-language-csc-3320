#define MAXNUM  10
#define MAXNAME 30

typedef struct enode *Enodeptr;
typedef struct snode *Snodeptr;
typedef struct cnode *Cnodeptr;

struct studentType {
  char sid[MAXNUM];
  char sname[MAXNAME];
};

struct courseType {
  char cno[MAXNUM];
  char title[MAXNAME];
  int hours;
};

struct snode {
  struct studentType student;
  Enodeptr firstE;
  Snodeptr next,prior;
};

struct cnode {
  struct courseType course;
  Enodeptr firstE;
  Cnodeptr next,prior;
};

struct enode {
  Enodeptr sNext, cNext;
  Snodeptr sOwner;
  Cnodeptr cOwner;
  char grade;
};

