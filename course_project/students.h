Snodeptr sAlloc(void);
/* Allocates storage for a snode and returns pointer to the structure */

void makenullStudents(Snodeptr *students);
/* Creates an empty list of students and returns pointer to dummy node
   in the variable students */

int insertStudents(struct studentType x, Snodeptr students);
/* inserts the student structure x in the student list; returns 0
   if successful and 1 if failed */

int deleteStudents(struct studentType x, Snodeptr students, Cnodeptr courses);
/* deletes the student structure x in the student list; returns 0
   if successful and 1 if failed */

int memberStudents(struct studentType x, Snodeptr students, Snodeptr *s);
/* returns 0 if student structure x is present in students list
   and 1 otherwise. It also returns a pointer (in s) to the snode where it
   found x; If x is not found it should return a pointer to the snode 
   where x should be inserted */
 
void printStudents(Snodeptr students);
/* prints the list of students (sid and names) in pretty format with
   -more- like option */

