Cnodeptr cAlloc(void);
/* Allocates storage for a cnode and returns pointer to the structure */

void makenullCourses(Cnodeptr *courses);
/* Creates an empty list of courses and returns pointer to dummy node
   in the variable courses */

int insertCourses(struct courseType x, Cnodeptr courses);
/* inserts the course structure x in the course list; returns 0
   if successful and 1 if failed */

int deleteCourses(struct courseType x, Snodeptr students, Cnodeptr courses);
/* deletes the course structure x in the course list; returns 0
   if successful and 1 if failed */

int memberCourses(struct courseType x, Cnodeptr courses, Cnodeptr *c);
/* returns 0 if course structure x is present in course list
   and 1 otherwise. It also returns a pointer (in c) to the cnode where it
   found x; If x is not found it should return a pointer to the cnode 
   where x should be inserted */
 
void printCourses(Cnodeptr courses);
/* prints the list of courses (cno, title and hours) in pretty format with
   -more- like option */
