#include <stdio.h>

int main(){

	int p = 0;
	int m = 0;
	int n = 13;
	int a = 1;
	int b = 2;

	for(p = 1, m = n, a = b; m > 0 && m % 2 != 0; p *= a, m = m/2, a *= a);  

	printf("%d-%d-%d-%d-%d\n", p, m, n, a, b); 	
}
