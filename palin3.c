#include <stdio.h>

/*
Author: Allan Hawkins 
Clase: CSC3320
Prof.: Xi He

This program checks if a phrase is palindrome using the name of an array
as a pointer
*/

int main(){
    

	char str[100];
	char str2[100];
	int i = 0;
	int j =0;
	int result = 0;
    	char ch;

    	printf("Enter posible palindrome: ");
    
    	while((ch = getchar()) != '\n'){
		if(ch < 65 || (ch > 90 && ch < 97) || ch > 122)
			ch = getchar();
		if(ch == '\n')
		break;
		*(str+i) = ch;
		i++;
	}       	
	*(str+i) = '\0';
	
	for(j=0,i-=1;*(str+j) != '\0';j++,i--){
		*(str2+j) = *(str+i);
	}
	*(str2+j) = '\0';


    	for(i=0,j=0;(*(str+j) != '\0') && (*(str2+i) != '\0');j++,i++){
        	if(*(str+j) != *(str2+i)){
            		result=1;
       		 }		
   	}	
        
    	if(result == 0)
        	printf("Palindrome\n");
   	else
        	printf("Not a palindrome\n");

}
