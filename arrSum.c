#include <stdio.h>
/*
This program reads a 5x5 array of integers and sums the rows and columns
Author: Allan Hawkins
Class: CSC3320
Prof.: Xi He
*/

int main(){

	int arr[5][5];
	char row1, row2, row3, row4, row5;
	int sRow1, sRow2, sRow3, sRow4, sRow5;
	int sCol1, sCol2, sCol3, sCol4, sCol5;
	int i=0;

	printf("Enter 5 numbers for row 1: ");
	for(i=0;i<5;i++)
	scanf("%d", &arr[0][i]);
	printf("Enter 5 numbers for row 2: ");
	for(i=0;i<5;i++)
	scanf("%d", &arr[1][i]);
	printf("Enter 5 numbers for row 3: ");
	for(i=0;i<5;i++)
	scanf("%d", &arr[2][i]);
	printf("Enter 5 numbers for row 4: ");
	for(i=0;i<5;i++)
	scanf("%d", &arr[3][i]);
	printf("Enter 5 numbers for row 5: ");
	for(i=0;i<5;i++)
	scanf("%d", &arr[4][i]);
	
	sRow1=0;
	for(i=0;i<5;i++)
	sRow1+=arr[0][i];
	sRow2=0;
	for(i=0;i<5;i++)
	sRow2+=arr[1][i];
	sRow3=0;
	for(i=0;i<5;i++)
	sRow3+=arr[2][i];
	sRow4=0;
	for(i=0;i<5;i++)
	sRow4+=arr[3][i];
	sRow5=0;
	for(i=0;i<5;i++)
	sRow5+=arr[4][i];
	
	sCol1=0;
	for(i=0;i<5;i++)
	sCol1+=arr[i][0];
	sCol2=0;
	for(i=0;i<5;i++)
	sCol2+=arr[i][1];
	sCol3=0;
	for(i=0;i<5;i++)
	sCol3+=arr[i][2];
	sCol4=0;
	for(i=0;i<5;i++)
	sCol4+=arr[i][3];
	sCol5=0;
	for(i=0;i<5;i++)
	sCol5+=arr[i][4];

	printf("Row totals: %d %d %d %d %d\n", sRow1, sRow2, sRow3, sRow4, sRow5);
	printf("Column Totals: %d %d %d %d %d\n", sCol1, sCol2, sCol3, sCol4, sCol5);

return 0;
}
