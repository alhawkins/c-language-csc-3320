#include <stdio.h>

	int main(void){

	int item;
	float price;
	int month;
	int day;
	int year;	

	printf("Enter item number: ");
	scanf("%d",&item);
	printf("Enter unit price: ");
	scanf("%f",&price);
	printf("Enter purchase date (mm/dd/yy): ");
	scanf("%d/%d/%d", &month, &day, &year);

	printf("Item\tUnit\tPurchase\n\tPrice\tDate\n");
	printf("%d\t%.2f\t%d/%d/%d\n", item, price, month, day, year);
	
	return 0;

}
