#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){

	char arr[10][10];
	int i,k;
	int dir;
	int r=0;
	int c=0;
	char letter ='A';
	srand((unsigned) time(NULL));	

	for(i=0;i<10;i++){
	for(k=0;k<10;k++)
	arr[i][k]='.';
	}

	arr[r][c]=letter;	
	dir = rand() % 4;	
	
	while(letter<'Z'){
	dir = rand() % 4;	
	
		if((r>0)&&(r<9)&&(c>0)&&(c<9)){	
			if(arr[r-1][c]!='.'&&arr[r+1][c]!='.'&&arr[r][c-1]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((r>0)&&(r<9)&&(c==0)){	
			if(arr[r-1][c]!='.'&&arr[r+1][c]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((r>0)&&(r<9)&&(c==9)){	
			if(arr[r-1][c]!='.'&&arr[r+1][c]!='.'&&arr[r][c-1]!='.')
			break;
		}
		else if((c>0)&&(c<9)&&(r==0)){	
			if(arr[r+1][c]!='.'&&arr[r][c-1]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((c>0)&&(c<9)&&(r==9)){	
			if(arr[r-1][c]!='.'&&arr[r][c-1]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((r==0)&&(c==0)){	
			if(arr[r+1][c]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((r==0)&&(c==9)){	
			if(arr[r+1][c]!='.'&&arr[r][c-1]!='.')
			break;
		}
		else if((r==9)&&(c==0)){	
			if(arr[r-1][c]!='.'&&arr[r][c+1]!='.')
			break;
		}
		else if((r==9)&&(c==9)){	
			if(arr[r-1][c]!='.'&&arr[r][c-1]!='.')
			break;
		}

		if(dir==0){                             //move up
			if((r>0)&&(arr[r-1][c]=='.'))
			arr[--r][c]=++letter;		
		}	
	
		else if(dir==1){			//move down
			if((r<9)&&(arr[r+1][c]=='.'))
			arr[++r][c]=++letter;
		}
	
		else if(dir==2){			//move left
			if((c>0)&&(arr[r][c-1]=='.'))
			arr[r][--c]=++letter;
		}
	
		else if(dir==3){			//move right
			if((c<9)&&(arr[r][c+1]=='.'))
			arr[r][++c]=++letter;
		}
	}
	
	
	for(i=0;i<10;i++){
	for(k=0;k<10;k++){
	printf("%c", arr[i][k]);
	}
	printf("\n");
	}
return 0;
}
