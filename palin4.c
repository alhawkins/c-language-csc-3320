#include <stdio.h>

/*
Author: Allan Hawkins 
Class: CSC3302
Prof.: Xi He

This progran checks if a phrase is a palindrome.
*/

int palindrome(char *s){

	char strcpy[100];
	int i=0;
	int j=0;
	int k=0;
	char ch;

	while((ch = getchar()) != '\n'){
		if(ch == ' '){
			ch = getchar();
		}
		*s=ch;
		s++;
		i++;
	}	
	*s='\0';
	s--;
		
	while(i>0){

		strcpy[j] = *s;
		s--;
		i--;
		j++;
	}
	strcpy[j+1]='\0';
	s++;
	//printf("strcpy = %s\n", strcpy);

	while((strcpy[k] != '\0') && (*s != '\0')){

		if(strcpy[k]>*s)
			return 1;
		else if(strcpy[k]<*s)
			return -1;
		s++;
		k++;
	}
	
	if(strcpy[k] != '\0')
		return 1;
	else if(*s != '\0')
		return -1;
	else
		return 0;

}

int main(){

	char str[100];
 
	printf("Please enter your possible palindrome: ");
	palindrome(str);
	//printf("str = %s\n", str);

	if(palindrome(str) == 0)	
		printf("Yes, your phrase is a palindrome! \n");
	else	
		printf("Your phrase is not a palindrome. \n");

return 0;

}
